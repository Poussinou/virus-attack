package com.kokkle.gametowerdefense;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.kokkle.gametowerdefense.stages.AbstractStage;
import com.kokkle.gametowerdefense.stages.GameStage;
import com.kokkle.gametowerdefense.stages.MenuStage;

public class MainGame extends ApplicationAdapter {

	private AbstractStage[] stages = new AbstractStage[2];
	private OrthographicCamera camera;
	private SpriteBatch batch;

	@Override
	public void create() {
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		stages[0] = new MenuStage(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()), batch);

		GameStage gameStage = new GameStage(new ExtendViewport(6000f, 6000f, camera), batch);
//        GameStage gameStage = new GameStage(
//                new ScalingViewport(Scaling.fillX, 2816,
//                        2816, camera), batch);
		stages[1] = gameStage;
		GameMaster.setStages(stages);
		GameMaster.changeStage(true);
	}

	@Override
	public void pause() {
		Gdx.input.cancelVibrate();
		super.pause();
	}

	@Override
	public void resume() {
		// TODO
		super.resume();
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(48f / 256, 110f / 256, 205f / 256, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		stages[GameMaster.getActiveStage()].act(Gdx.graphics.getDeltaTime());
		stages[GameMaster.getActiveStage()].draw();
		// stages[GameMaster.getActiveStage()].getViewport().apply();
	}

	@Override
	public void resize(int width, int height) {
		// float aspectRatio = (float) width / (float) height;
		// camera = new OrthographicCamera(2f * aspectRatio, 2f);
		// camera.setToOrtho(false, width, height);
		// camera.update();
		for (Stage stage : stages) {
			stage.getViewport().setScreenSize(width, height);
		}
	}

	@Override
	public void dispose() {
		for (Stage stage : stages) {
			stage.dispose();
		}
		batch.dispose();
	}
}
