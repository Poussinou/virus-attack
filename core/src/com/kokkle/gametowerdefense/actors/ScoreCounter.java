package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kokkle.gametowerdefense.logic.SharedKillCounter;

public class ScoreCounter extends Actor {

	private BitmapFont infoFont = new BitmapFont();
	private SharedKillCounter sharedKillCounter;

	public ScoreCounter(SharedKillCounter sharedKillCounter) {
		this.sharedKillCounter = sharedKillCounter;
		infoFont.getData().setScale(8.5f);
		infoFont.setColor(Color.BLUE);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		infoFont.draw(batch, "Score: " + sharedKillCounter.getKillCounter(), 20, 200);
		super.draw(batch, parentAlpha);
	}

}
