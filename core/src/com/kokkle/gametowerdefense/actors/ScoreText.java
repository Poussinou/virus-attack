package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kokkle.gametowerdefense.logic.prefs.HighscorePref;

public class ScoreText extends Actor {

	private HighscorePref highscorePref;
	private BitmapFont highscoreText = new BitmapFont();
	private float density = Gdx.graphics.getDensity();

	public ScoreText() {
		// highscoreText.getData().setScale(1f);
		highscoreText.setColor(Color.BLACK);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (highscorePref != null) {
			highscoreText.draw(batch, "Best Score: " + highscorePref.getScore(), // TODO not shown
					Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
					Gdx.graphics.getHeight() / 2 - (10f + (density - 1) * 10f));
		} else {
			highscoreText.draw(batch, "No score yet", Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
					Gdx.graphics.getHeight() / 2 - (10f + (density - 1) * 10f));
		}
		super.draw(batch, parentAlpha);
	}

	public void setScore(HighscorePref highscorePref) {
		this.highscorePref = highscorePref;
	}

}
