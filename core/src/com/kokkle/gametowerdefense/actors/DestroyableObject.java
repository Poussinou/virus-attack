package com.kokkle.gametowerdefense.actors;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kokkle.gametowerdefense.GameMaster;

/**
 * Created by anstue on 11.03.20.
 */
public abstract class DestroyableObject extends Actor {

	protected float health;
	protected float maxHealth;
	private boolean enemy;
	protected List<Damager> damagers = new ArrayList<>();
	protected Texture texture;
	protected Body body;
	protected World world;

	public DestroyableObject(float x, float y, float density, float health, boolean enemy, String texturePath,
			World world, BodyDef.BodyType bodyType, boolean physicalCircle) {
		super();
		this.maxHealth = health;
		this.health = health;
		this.enemy = enemy;
		this.world = world;
		texture = new Texture(texturePath);
		setPosition(x - texture.getWidth() / 2, y - texture.getHeight() / 2);
		setBounds(x - texture.getWidth() / 2, y - texture.getHeight() / 2, texture.getWidth(), texture.getHeight());
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(
				getX() / GameMaster.conversionPhysicsPixel + getWidth() / GameMaster.conversionPhysicsPixel / 2,
				getY() / GameMaster.conversionPhysicsPixel + getHeight() / GameMaster.conversionPhysicsPixel / 2);

		body = world.createBody(bodyDef);
		body.setFixedRotation(true);

		Shape shape;
		if (physicalCircle) {
			shape = new CircleShape();
			((CircleShape) shape).setRadius(getWidth() / 4 / GameMaster.conversionPhysicsPixel);
		} else {
			shape = new PolygonShape();
			((PolygonShape) shape).setAsBox(getWidth() / GameMaster.conversionPhysicsPixel / 4,
					getHeight() / GameMaster.conversionPhysicsPixel / 4);
		}
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = density;

		Fixture fixture = body.createFixture(fixtureDef);
		fixture.setUserData(this);

		shape.dispose();
	}

	public void giveDamage(float damage) {
		this.health -= damage;
	}

	public boolean isDead() {
		return health <= 0;
	}

	public void dispose() {
		texture.dispose();
		world.destroyBody(body);
	}

	public boolean isEnemy() {
		return enemy;
	}

	public float getHealth() {
		return health;
	}

	public void addHealth(float amount) {
		if (health + amount > maxHealth) {
			health = maxHealth;
		} else {
			health += amount;
		}
	}

	@Override
	public void act(float delta) {
		this.setPosition(body.getPosition().x * GameMaster.conversionPhysicsPixel - getWidth() / 2,
				body.getPosition().y * GameMaster.conversionPhysicsPixel - getHeight() / 2);
		List<Damager> list = new ArrayList<>(damagers);
		for (int i = 0; i < list.size(); i++) {
			health = health - list.get(i).damage() * delta;
		}
		super.act(delta);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		batch.draw(texture, getX(), getY());
	}

	public void addDamager(Damager damager) {
		damagers.add(damager);

	}

	public void removeDamager(Damager damager) {
		damagers.remove(damager);
	}

}
