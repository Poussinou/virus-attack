package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by anstue on 11.03.20.
 */
public class Tower extends DestroyableObject {

	private static final int HEALTHBAR_LENGTH = 500;
	private BitmapFont infoFont = new BitmapFont();
	private boolean vibrateActive = false;
	private Texture textureHealth;
	private Texture textureNoneHealth;

	public Tower(World world, float worldWidth, float worldHeight) {
		super(worldWidth / 2, worldHeight / 2, 1f, 10f, false, "tower.png", world, BodyDef.BodyType.StaticBody, true);
		infoFont.getData().setScale(8.5f);
		infoFont.setColor(Color.BLUE);
		Pixmap health = createProceduralPixmap(HEALTHBAR_LENGTH, 10, 0, 1, 0);
		Pixmap noneHealth = createProceduralPixmap(HEALTHBAR_LENGTH, 10, 1, 0, 0);
		textureHealth = new Texture(health);
		textureNoneHealth = new Texture(noneHealth);
	}

	@Override
	public void act(float delta) {
		if (damagers.size() > 0 && !vibrateActive) {
			Gdx.input.vibrate(new long[] { 0, 200, 100 }, 0);
			vibrateActive = true;
		} else if (damagers.size() == 0) {
			Gdx.input.cancelVibrate();
			vibrateActive = false;
		}
		super.act(delta);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		// infoFont.draw(batch, Math.floor(this.getHealth() * 100) / 100 + "", getX() +
		// getWidth() / 2,
		// getY() + getHeight() / 2);

		batch.draw(textureNoneHealth, getX() + 150, getY(), HEALTHBAR_LENGTH, 20);
		batch.draw(textureHealth, getX() + 150, getY(), health / maxHealth * HEALTHBAR_LENGTH, 20);

	}

	private Pixmap createProceduralPixmap(int width, int height, int r, int g, int b) {
		Pixmap pixmap = new Pixmap(width, height, Format.RGBA8888);

		pixmap.setColor(r, g, b, 1);
		pixmap.fill();

		return pixmap;
	}

}
