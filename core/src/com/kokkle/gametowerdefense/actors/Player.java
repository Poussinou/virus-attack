package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.kokkle.gametowerdefense.GameMaster;
import com.kokkle.gametowerdefense.objects.Weapon;

/**
 * Created by anstue on 11.03.20.
 */
public class Player extends DestroyableObject {

	private Inventory inventory;
	private Weapon activeWeapon;
	private float moveTime = 0;
	private boolean isMoving = false;
	private Tower tower;

	public Player(World world, Tower tower) {
		super(530f, 530f, 1f, 1, false, "player.png", world, BodyDef.BodyType.DynamicBody, false);
		this.world = world;
		this.tower = tower;

	}

	// TODO weapon can have a sword or bow
	// TODO hitting on the touchscreen hits or shoots an arrow
	// TODO touchDown for longer moves player

	@Override
	public void act(float delta) {
		checkMovementAndStop(delta);
		super.act(delta);
	}

	private void checkMovementAndStop(float delta) {
		if (isMoving) {
			if (moveTime > 1) {
				body.setLinearVelocity(0, 0);
				isMoving = false;
			}
			moveTime += delta;
		}
	}

	public void newTargetLocation(float x, float y) {

		Vector2 targetPoint = new Vector2(x / GameMaster.conversionPhysicsPixel, y / GameMaster.conversionPhysicsPixel);
		body.setLinearVelocity(0, 0);
		body.applyForce(calculateVelocity(targetPoint), body.getWorldCenter(), true);
	}

	public Vector2 calculateVelocity(Vector2 target) {
		Vector2 direction = new Vector2(
				target.x - body.getPosition().x + getWidth() / GameMaster.conversionPhysicsPixel / 2,
				target.y - body.getPosition().y + getHeight() / GameMaster.conversionPhysicsPixel / 2).nor();
		float speed = 55;
		return new Vector2(speed * direction.x, speed * direction.y);
	}

	private double getEuclidDistance(float x, float y) {
		return Math.sqrt(Math.pow(x - getX(), 2) + Math.pow(y - getY(), 2));
	}

	public void useHealthPack() {
		tower.addHealth(1f);

	}

}
