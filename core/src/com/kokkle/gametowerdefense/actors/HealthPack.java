package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class HealthPack extends DestroyableObject {

	private float timeToLife;

	public HealthPack(float x, float y, World world, float timeToLife) {
		super(x, y, 0.5f, 1, false, "healthPack.png", world, BodyDef.BodyType.DynamicBody, false);
		this.timeToLife = timeToLife;
	}

	@Override
	public void act(float delta) {
		timeToLife -= delta;
		if (timeToLife <= 0) {
			health = 0;
		}
		super.act(delta);
	}

}
