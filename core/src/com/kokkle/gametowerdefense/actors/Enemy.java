package com.kokkle.gametowerdefense.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by anstue on 11.03.20.
 */
public class Enemy extends DestroyableObject implements Damager {

	private float speed;
	private Vector2 target;

	public Enemy(float x, float y, World world, Vector2 target, float speed) {
		super(x, y, 0.5f, 1, true, "enemy.png", world, BodyDef.BodyType.DynamicBody, false);
		this.speed = speed;
		this.target = target;
		body.applyForce(calculateVelocity(target), body.getWorldCenter(), true);

	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}

	public Vector2 calculateVelocity(Vector2 target) {
		Vector2 direction = new Vector2(target.x - body.getPosition().x, target.y - body.getPosition().y).nor();
		return new Vector2(speed * direction.x, speed * direction.y);
	}

	@Override
	public float damage() {
		return 0.1f;
	}

}
