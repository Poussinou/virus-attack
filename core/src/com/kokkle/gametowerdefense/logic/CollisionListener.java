package com.kokkle.gametowerdefense.logic;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.kokkle.gametowerdefense.actors.Damager;
import com.kokkle.gametowerdefense.actors.DestroyableObject;
import com.kokkle.gametowerdefense.actors.Enemy;
import com.kokkle.gametowerdefense.actors.HealthPack;
import com.kokkle.gametowerdefense.actors.Player;
import com.kokkle.gametowerdefense.actors.Tower;

public class CollisionListener implements ContactListener {

	private SharedKillCounter sharedKillCounter;

	public CollisionListener(SharedKillCounter sharedKillCounter) {
		this.sharedKillCounter = sharedKillCounter;
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		// Player collisions
		if (fixtureA.getUserData() instanceof Player) {
			if (fixtureB.getUserData() instanceof DestroyableObject
					&& ((DestroyableObject) fixtureB.getUserData()).isEnemy()) {
				((DestroyableObject) fixtureB.getUserData()).giveDamage(1f);
				sharedKillCounter.increase();
			}
		} else if (fixtureB.getUserData() instanceof Player) {
			if (fixtureB.getUserData() instanceof DestroyableObject
					&& ((DestroyableObject) fixtureB.getUserData()).isEnemy()) {
				((DestroyableObject) fixtureB.getUserData()).giveDamage(1f);
				sharedKillCounter.increase();
			}
		}
		// Tower collisions
		else if (fixtureA.getUserData() instanceof Enemy && fixtureB.getUserData() instanceof Tower) {
			((DestroyableObject) fixtureB.getUserData()).addDamager((Damager) fixtureA.getUserData());
		} else if (fixtureB.getUserData() instanceof Enemy && fixtureA.getUserData() instanceof Tower) {
			((DestroyableObject) fixtureA.getUserData()).addDamager((Damager) fixtureB.getUserData());
		}

		// HealthPack collisions
		if (fixtureA.getUserData() instanceof HealthPack && fixtureB.getUserData() instanceof Player) {
			((Player) fixtureB.getUserData()).useHealthPack();
			((DestroyableObject) fixtureA.getUserData()).giveDamage(1f);
		} else if (fixtureB.getUserData() instanceof HealthPack && fixtureA.getUserData() instanceof Player) {
			((Player) fixtureA.getUserData()).useHealthPack();
			((DestroyableObject) fixtureB.getUserData()).giveDamage(1f);
		}
	}

	@Override
	public void endContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		// Tower collisions
		if (fixtureA.getUserData() instanceof Enemy && fixtureB.getUserData() instanceof Tower) {
			((DestroyableObject) fixtureB.getUserData()).removeDamager((Damager) fixtureA.getUserData());
		} else if (fixtureB.getUserData() instanceof Enemy && fixtureA.getUserData() instanceof Tower) {
			((DestroyableObject) fixtureA.getUserData()).removeDamager((Damager) fixtureB.getUserData());
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
