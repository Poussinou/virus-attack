package com.kokkle.gametowerdefense.logic.prefs;

public class HighscorePref {

	private int score = 0;
	private String date;

	public int getScore() {
		return score;
	}

	public String getDate() {
		return date;
	}

	public HighscorePref(int score, String date) {
		super();
		this.score = score;
		this.date = date;
	}

}
