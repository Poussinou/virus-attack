package com.kokkle.gametowerdefense.logic;

import java.util.Random;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kokkle.gametowerdefense.actors.HealthPack;

public class HealthPackSpawner {

	private Stage stage;
	private int minIdleTime;
	private Random random = new Random();
	private int maxIdleTime;
	private World world;

	private float timeCounter = 4;

	public HealthPackSpawner(Stage stage, int minIdleTime, int maxIdleTime, World world) {
		super();
		this.stage = stage;
		this.minIdleTime = minIdleTime;
		this.maxIdleTime = maxIdleTime;
		this.world = world;
	}

	public void act(float delta) {
		if (timeCounter <= 0) {
			boolean leftOrRight = (random.nextInt(2) == 1);
			boolean upOrDown = (random.nextInt(2) == 1);
			HealthPack healthPack = new HealthPack((leftOrRight) ? 700 : stage.getViewport().getWorldWidth() - 700,
					(upOrDown) ? 700 : stage.getViewport().getWorldHeight() - 700, world, minIdleTime);

			this.stage.addActor(healthPack);
			timeCounter = random.nextInt(maxIdleTime + 1) + minIdleTime;
		}
		timeCounter -= delta;
	}

}
