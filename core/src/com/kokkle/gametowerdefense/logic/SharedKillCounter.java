package com.kokkle.gametowerdefense.logic;

public class SharedKillCounter {
	private int killCounter = 0;

	public int getKillCounter() {
		return killCounter;
	}

	public void increase() {
		killCounter++;

	}

}
