package com.kokkle.gametowerdefense.logic;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.kokkle.gametowerdefense.logic.prefs.HighscorePref;

/**
 * Created by anstue on 24.02.20.
 */
public class PreferenceManager {
	private static final String PREF_HIGHSCORE_DATE = "highscore_date";
	private static final String PREF_HIGHSCORE = "highscore";
	static Preferences prefs = Gdx.app.getPreferences("game_state");

// prefs.putInteger("option", MenuScreen.option);
//    prefs.flush();
	public void saveState() {
		// TODO game started
		// TODO highscore
		// TODO current score
		// TODO current level
	}

	public boolean setIfNewHighScore(int score) {
		if (!prefs.contains(PREF_HIGHSCORE) || prefs.getInteger(PREF_HIGHSCORE) <= score) {
			prefs.putInteger(PREF_HIGHSCORE, score);
			String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
			prefs.putString(PREF_HIGHSCORE_DATE, formattedDate);
			prefs.flush();
			return true;
		} else {
			return false;
		}
	}

	public Optional<HighscorePref> getHighscore() {
		if (prefs.contains(PREF_HIGHSCORE)) {
			return Optional
					.of(new HighscorePref(prefs.getInteger(PREF_HIGHSCORE), prefs.getString(PREF_HIGHSCORE_DATE)));
		} else {
			return Optional.empty();
		}
	}

}

//TODO campaign
//TODO free to play with high score

//TODO highscore menu
//TODO facebook/whatsappshare