package com.kokkle.gametowerdefense.logic;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kokkle.gametowerdefense.actors.Enemy;

public class EnemySpawner {

	private Stage stage;
	private float idleTime;
	private Random random = new Random();
	private int max;
	private World world;
	private Vector2 target;

	private float timeCounter = 0;

	public EnemySpawner(World world, Stage stage, float idleTime, int max, Vector2 target) {
		this.stage = stage;
		this.idleTime = idleTime;
		this.max = max;
		this.world = world;
		this.target = target;
	}

	public void act(float delta) {
		if (timeCounter <= 0) {
			int num = random.nextInt(max + 1) + 1;
			for (int i = 1; i <= num; i++) {
				int leftOrRight = random.nextInt(2);
				int startX = 512;
				if (leftOrRight == 1) {
					startX = (int) stage.getViewport().getWorldWidth() - 512;
				}
				Enemy enemy = new Enemy(startX - random.nextInt(256),
						random.nextInt((int) stage.getViewport().getWorldHeight()), world, target,
						random.nextInt(10) + 5);
				this.stage.addActor(enemy);
			}
			timeCounter = idleTime;
		}
		timeCounter -= delta;
	}

}
