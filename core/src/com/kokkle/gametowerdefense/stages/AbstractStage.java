package com.kokkle.gametowerdefense.stages;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class AbstractStage extends Stage {

	AbstractStage(Viewport viewport, SpriteBatch batch) {
		super(viewport, batch);
	}

	public void switchToStage() {

	}

	public void reset() {

	}
}
