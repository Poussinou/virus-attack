package com.kokkle.gametowerdefense.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kokkle.gametowerdefense.GameMaster;
import com.kokkle.gametowerdefense.actors.ScoreText;
import com.kokkle.gametowerdefense.logic.PreferenceManager;

public class MenuStage extends AbstractStage {
	public static final float BUTTON_HEIGHT = 40f;
	private Skin skin;
	private TextButton couchModeButton;
	private TextButton resumeButton;
	private TextButton newGameButton;
	private TextButton exitButton;
	private TextButton highscoreButton;
	private TextButton shareButton;
	private Texture headerTexture;
	private boolean highScoreScreen = false;
	private PreferenceManager preferenceManager;
	private SpriteBatch batch;
	private float density = Gdx.graphics.getDensity();
	private ScoreText scoreText;

	public MenuStage(Viewport viewport, SpriteBatch batch) {
		super(viewport, batch);
		this.batch = batch;
		headerTexture = new Texture("menuHeader.png");
		preferenceManager = new PreferenceManager();
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		createToStartScreen();
		createPauseMenu();
		createHighscoreMenu();
	}

	private void createHighscoreMenu() {
		shareButton = createButton("Share", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (90f + (density - 1) * 90f));

		shareButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO implement share functionality
			}

		});
		scoreText = new ScoreText();
		this.addActor(shareButton);
		this.addActor(scoreText);

	}

	private void createToStartScreen() {
		couchModeButton = createButton("New Game", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (10f + (density - 1) * 10));

		couchModeButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameMaster.setToNewGameStage();
			}

		});

		highscoreButton = createButton("Highscore", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (50f + (density - 1) * 50));

		highscoreButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setToHighscoreScreen();
			}

		});

		exitButton = createButton("Exit Game", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (90f + (density - 1) * 90f));

		exitButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}

		});

		this.addActor(couchModeButton);
		this.addActor(exitButton);
		this.addActor(highscoreButton);
	}

	private TextButton createButton(String text, float width, float x, float y) {

		final TextButton button = new TextButton(text, skin, "default");
		button.setVisible(false);
		button.setWidth(width);
		button.setHeight(BUTTON_HEIGHT);
		button.setPosition(x, y);
		button.setBounds(x, y, width, BUTTON_HEIGHT);
		button.setTransform(true);
		button.setScale(density);
		return button;
	}

	@Override
	public void switchToStage() {
		Gdx.input.setInputProcessor(this);
		if (GameMaster.gameRunning) {
			setPauseMenu();
		} else {
			setToStartScreen();
		}
		Gdx.input.setCatchKey(Input.Keys.BACK, true);
		if (preferenceManager.getHighscore().isPresent()) {
			scoreText.setScore(preferenceManager.getHighscore().get());
			shareButton.setText("Score: " + preferenceManager.getHighscore().get().getScore());
		}
		super.switchToStage();
	}

	private void createPauseMenu() {

		resumeButton = createButton("Resume", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (10f + (density - 1) * 10f));
		newGameButton = createButton("New Game", 200f, Gdx.graphics.getWidth() / 2 - (100f + (density - 1) * 100f),
				Gdx.graphics.getHeight() / 2 - (50f + (density - 1) * 50f));
		resumeButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameMaster.resumeGameStage();
			}
		});
		newGameButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameMaster.gameRunning = false;
				setToStartScreen();
			}
		});
		this.addActor(resumeButton);
		this.addActor(newGameButton);
	}

	private void setPauseMenu() {
		makeAllInvisible();
		resumeButton.setVisible(true);
		newGameButton.setVisible(true);
		exitButton.setVisible(true);
	}

	private void setToStartScreen() {
		makeAllInvisible();
		couchModeButton.setVisible(true);
		highscoreButton.setVisible(true);
		exitButton.setVisible(true);
	}

	private void setToHighscoreScreen() {
		makeAllInvisible();
		highScoreScreen = true;
		shareButton.setVisible(true);
		//scoreText.setVisible(true);
	}

	private void makeAllInvisible() {
		couchModeButton.setVisible(false);
		resumeButton.setVisible(false);
		newGameButton.setVisible(false);
		exitButton.setVisible(false);
		highscoreButton.setVisible(false);
		shareButton.setVisible(false);
		scoreText.setVisible(false);
		highScoreScreen = false;

	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
			if (highScoreScreen) {
				switchToStage();
			} else {
				Gdx.app.exit();
			}
		}

		return false;
	}

	@Override
	public void draw() {
		batch.begin();
		batch.draw(headerTexture, 0, 0);
		batch.end();
		super.draw();
	}

}
