package com.kokkle.gametowerdefense.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kokkle.gametowerdefense.GameMaster;
import com.kokkle.gametowerdefense.actors.DestroyableObject;
import com.kokkle.gametowerdefense.actors.Player;
import com.kokkle.gametowerdefense.actors.ScoreCounter;
import com.kokkle.gametowerdefense.actors.Tower;
import com.kokkle.gametowerdefense.logic.CollisionListener;
import com.kokkle.gametowerdefense.logic.EnemySpawner;
import com.kokkle.gametowerdefense.logic.HealthPackSpawner;
import com.kokkle.gametowerdefense.logic.PreferenceManager;
import com.kokkle.gametowerdefense.logic.SharedKillCounter;

/**
 * Created by anstue on 08.02.20.
 */
public class GameStage extends AbstractStage implements GestureDetector.GestureListener {

	private OrthographicCamera cam;
	private InputMultiplexer inputMultiplexer = new InputMultiplexer();
	private World world;
	private Player player;
	private Box2DDebugRenderer debugRenderer;
	private Matrix4 debugMatrix;
	private EnemySpawner enemySpawner;
	private HealthPackSpawner healthPackSpawner;
	private Tower tower;
	private boolean gameOver;
	private TextButton gameOverButton;
	private final Skin skin;
	private SharedKillCounter sharedKillCounter;

	@Override
	public void switchToStage() {
		Gdx.input.setCatchKey(Input.Keys.BACK, true);
		Gdx.input.setInputProcessor(inputMultiplexer);
		super.switchToStage();

	}

	private float initialScale = 1;

	public GameStage(Viewport viewport, SpriteBatch batch) {
		super(viewport, batch);
		this.cam = (OrthographicCamera) viewport.getCamera();
		inputMultiplexer.addProcessor(this);
		inputMultiplexer.addProcessor(new GestureDetector(this));
		debugMatrix = new Matrix4(cam.combined);

		debugMatrix.scale(GameMaster.conversionPhysicsPixel, GameMaster.conversionPhysicsPixel, 1f);
		debugRenderer = new Box2DDebugRenderer();
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));

	}

	private TextButton createButton(String text, float width, float x, float y) {

		final TextButton button = new TextButton(text, skin, "default");
		button.setVisible(false);
		button.setWidth(width);
		button.setHeight(40f);
		button.setPosition(x, y);
		button.setBounds(x, y, width, 40f);
		button.setTransform(true);
		button.setScale(8.5f);
		return button;
	}

	public void initialize() {
		this.getActors().clear();
		sharedKillCounter = new SharedKillCounter();
		world = new World(new Vector2(0, 0), true);
		world.setContactListener(new CollisionListener(sharedKillCounter));
		tower = new Tower(world, this.getViewport().getWorldWidth(), this.getViewport().getWorldHeight());
		enemySpawner = new EnemySpawner(world, this, 3, 5, getTarget(tower));
		healthPackSpawner = new HealthPackSpawner(this, 5, 25, world);
		player = new Player(world, tower);
		this.addActor(player);
		this.addActor(tower);
		ScoreCounter scoreCounter = new ScoreCounter(sharedKillCounter);
		this.addActor(scoreCounter);
		gameOver = false;
		createScreenBorder();

		gameOverButton = createButton("Game Over", 200f, this.getViewport().getWorldWidth() / 2 - 200f,
				this.getViewport().getWorldHeight() / 2 - 10f);
		gameOverButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				GameMaster.gameRunning = false;
				GameMaster.openMenuStage();
			}
		});

		this.addActor(gameOverButton);
	}

	private void createScreenBorder() {
		createWall(0, 0, 0, this.getViewport().getWorldHeight() / GameMaster.conversionPhysicsPixel);
		createWall(0, 0, this.getViewport().getWorldWidth() / GameMaster.conversionPhysicsPixel, 0);
		// top
		createWall(0, this.getViewport().getWorldHeight() / GameMaster.conversionPhysicsPixel,
				this.getViewport().getWorldWidth() / GameMaster.conversionPhysicsPixel, 0);
		createWall(this.getViewport().getWorldWidth() / GameMaster.conversionPhysicsPixel, 0, 0,
				this.getViewport().getWorldHeight() / GameMaster.conversionPhysicsPixel);
	}

	private void createWall(float x, float y, float hightX, float hightY) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.StaticBody;
		bodyDef.position.set(x, y);

		Body body = world.createBody(bodyDef);
		body.setFixedRotation(true);
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(hightX, hightY);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 1f;

		Fixture fixture = body.createFixture(fixtureDef);
		fixture.setUserData(this);

		shape.dispose();
	}

	private Vector2 getTarget(Tower tower) {
		return new Vector2(
				tower.getX() / GameMaster.conversionPhysicsPixel
						+ tower.getWidth() / GameMaster.conversionPhysicsPixel / 2,
				tower.getY() / GameMaster.conversionPhysicsPixel
						+ tower.getHeight() / GameMaster.conversionPhysicsPixel / 2);
	}

	@Override
	public void act(float delta) {
		world.step(Gdx.graphics.getDeltaTime(), 6, 2);
		for (int i = 0; i < this.getActors().size; i++) {
			if (this.getActors().get(i) instanceof DestroyableObject) {
				if (((DestroyableObject) this.getActors().get(i)).isDead()) {
					((DestroyableObject) this.getActors().get(i)).dispose();
					this.getActors().get(i).remove();
				}
			}
		}
		if (!gameOver && this.getActors().size < 100) {
			enemySpawner.act(delta);
			healthPackSpawner.act(delta);
		}
		if (tower.getHealth() <= 0 && !gameOver) {
			gameOver = true;
			gameOverButton.setVisible(true);
			PreferenceManager preferenceManager = new PreferenceManager();
			if(!preferenceManager.getHighscore().isPresent() || preferenceManager.getHighscore().get().getScore()<sharedKillCounter.getKillCounter()) {
				preferenceManager.setIfNewHighScore(sharedKillCounter.getKillCounter());
			}
		}
		super.act(delta);
	}

	@Override
	public void draw() {
		super.draw();
		debugRenderer.render(world, debugMatrix);
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return super.touchDragged(screenX, screenY, pointer);
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
			GameMaster.switchToPauseMenu();
		}

		return false;
	}

	@Override
	public void dispose() {
		Gdx.input.cancelVibrate();
		super.dispose();
	}

	@Override
	public void reset() {
		this.initialize();
		super.reset();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		System.out.println("TouchDown " + x + " " + y);
		initialScale = cam.zoom;
		Vector2 newPoints = new Vector2(x, y);
		newPoints = getViewport().unproject(newPoints);
		if (!gameOver) {
			player.newTargetLocation(newPoints.x, newPoints.y);
		}
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}

}
