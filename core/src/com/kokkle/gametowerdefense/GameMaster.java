package com.kokkle.gametowerdefense;

import com.kokkle.gametowerdefense.stages.AbstractStage;

/**
 * Created by anstue on 08.02.20.
 */
public class GameMaster {
	public static boolean gameRunning = false;
	public static final int conversionPhysicsPixel = 512;
	private static int activeStage = 0;
	private static AbstractStage stages[];

	public static void setToNewGameStage() {
		gameRunning = true;
		activeStage = 1;
		changeStage(true);
	}

	public static void resumeGameStage() {
		activeStage = 1;
		changeStage(false);
	}

	public static void changeStage(boolean reset) {
		stages[activeStage].switchToStage();
		if (reset) {
			stages[activeStage].reset();
		}
	}

	public static void openMenuStage() {
		activeStage = 0;
		changeStage(true);
	}


	public static int getActiveStage() {
		return activeStage;
	}

	public static AbstractStage[] getStages() {
		return stages;
	}

	public static void setStages(AbstractStage[] stages) {
		GameMaster.stages = stages;
	}

	public static void switchToPauseMenu() {
		activeStage = 0;
		changeStage(true);

	}

}
