# Virus-attack game

Is a game set up in a microscopic environment, where you control a immun system cell. 

### F-Droid setup

`../../fdroidserver/fdroid readmeta`

`../../fdroidserver/fdroid rewritemeta com.kokkle.gamevirusattack`

`../../fdroidserver/fdroid checkupdates com.kokkle.gamevirusattack`

`../../fdroidserver/fdroid lint com.kokkle.gamevirusattack`

`../../fdroidserver/fdroid build -v -l com.kokkle.gamevirusattack`